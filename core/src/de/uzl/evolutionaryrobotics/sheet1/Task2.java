package de.uzl.evolutionaryrobotics.sheet1;

import com.badlogic.gdx.math.Matrix4;

import de.upb.swarmrobotics.core.Environment;
import de.upb.swarmrobotics.core.Robot;
import de.upb.swarmrobotics.core.SensorPosition;
import de.upb.swarmrobotics.core.Task;
import de.upb.swarmrobotics.sensors.ProximitySensor;

public class Task2 implements Task {

	@Override
	public void onCreate(Environment environment) {
		Robot robot = environment.createRobot(0.5f);
		robot.attach(new ProximitySensor(SensorPosition.FrontLeft));
		robot.attach(new ProximitySensor(SensorPosition.Center));
		robot.attach(new ProximitySensor(SensorPosition.FrontRight));
		robot.initialPosition(5, 5);
		robot.attach(new ControlByRules());
	}

	@Override
	public void render(Matrix4 mvp) {
	}

}
