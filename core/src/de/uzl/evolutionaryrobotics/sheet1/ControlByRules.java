package de.uzl.evolutionaryrobotics.sheet1;

import de.upb.swarmrobotics.core.Behavior;
import de.upb.swarmrobotics.core.SensorPosition;
import de.upb.swarmrobotics.sensors.ProximitySensor;

public class ControlByRules extends Behavior {

	private enum Direction {
		LEFT(5), STRAIGHT(0), RIGHT(-5);
		public final float angularVelocity;

		Direction(float angle) {
			angularVelocity = angle;
		}
	}

	private ProximitySensor left, center, right;
	private final float PERIOD = .2f;
	private Direction direction = Direction.STRAIGHT;

	@Override
	public void onCreate() {
		super.onCreate();
		left = robot.sensor(ProximitySensor.class, SensorPosition.FrontLeft);
		center = robot.sensor(ProximitySensor.class, SensorPosition.Center);
		right = robot.sensor(ProximitySensor.class, SensorPosition.FrontRight);
		robot.setTargetSpeed(3);
	}

	@Override
	public void update(float delta) {
		super.update(delta);
		fireSensors(delta);
		chooseDirection(left.getValue(), center.getValue(), right.getValue());
		turnInDirection(direction);

	}

	private void fireSensors(float delta) {
		left.fireIfOverPeriod(delta, PERIOD);
		center.fireIfOverPeriod(delta, PERIOD);
		right.fireIfOverPeriod(delta, PERIOD);
	}

	private void chooseDirection(float left, float center, float right) {
		switch (direction) {
		case LEFT:
			if (center < .6f) {
				direction = Direction.STRAIGHT;
			}
			break;
		case RIGHT:
			if (center < .6f) {
				direction = Direction.STRAIGHT;
			}
			break;
		case STRAIGHT:
			if (left < right) {
				direction = Direction.LEFT;
			} else {
				direction = Direction.RIGHT;
			}
			break;

		}
	}

	private void turnInDirection(Direction direction) {
		robot.setAngularVelocity(direction.angularVelocity);
	}

}
