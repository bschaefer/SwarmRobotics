package de.uzl.evolutionaryrobotics.sheet1;

import de.upb.swarmrobotics.core.Behavior;
import de.upb.swarmrobotics.core.SensorPosition;
import de.upb.swarmrobotics.sensors.LightSensor;

public abstract class BraitenbergBehaviour extends Behavior {

	protected LightSensor left, right;
	protected int rotationFactor = 1000, speedFactor = 500;
	protected int maxRotation = 10, maxSpeed = 5;

	@Override
	public void onCreate() {
		left = robot.sensor(LightSensor.class, SensorPosition.FrontLeft);
		right = robot.sensor(LightSensor.class, SensorPosition.FrontRight);
	}

	@Override
	public void update(float delta) {
		robot.setAngularVelocity(
				Math.min(rotationFactor * delta * (rightMotorValue() - leftMotorValue()), maxRotation));
		robot.setTargetSpeed(Math.min(speedFactor * delta * (rightMotorValue() + leftMotorValue()), maxSpeed));
	}

	protected abstract float leftMotorValue();

	protected abstract float rightMotorValue();

}
