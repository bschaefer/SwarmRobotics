package de.uzl.evolutionaryrobotics.sheet1;

import com.badlogic.gdx.math.Matrix4;

import de.upb.swarmrobotics.core.Environment;
import de.upb.swarmrobotics.core.Robot;
import de.upb.swarmrobotics.core.SensorPosition;
import de.upb.swarmrobotics.core.Task;
import de.upb.swarmrobotics.sensors.LightSensor;

public class Task1 implements Task {

	@Override
	public boolean isTorus() {
		return true;
	}

	@Override
	public void onCreate(Environment environment) {
		createRobot(environment, 5, 5, new Fear());
		createRobot(environment, -5, -5, new Aggression());

		environment.createLightSource(0, 0);

	}

	private void createRobot(Environment environment, float x, float y, BraitenbergBehaviour behaviour) {
		Robot robot = environment.createRobot(0.5f);
		robot.attach(new LightSensor(environment, SensorPosition.FrontLeft));
		robot.attach(new LightSensor(environment, SensorPosition.FrontRight));
		robot.initialPosition(x, y);
		robot.attach(behaviour);
	}

	@Override
	public void render(Matrix4 mvp) {
	}

}
