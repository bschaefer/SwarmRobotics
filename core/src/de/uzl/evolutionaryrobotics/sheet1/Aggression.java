package de.uzl.evolutionaryrobotics.sheet1;

public class Aggression extends BraitenbergBehaviour {

	@Override
	protected float leftMotorValue() {
		return right.getValue();
	}

	@Override
	protected float rightMotorValue() {
		return left.getValue();
	}

}
