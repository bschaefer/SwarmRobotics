package de.uzl.evolutionaryrobotics.sheet1;

public class Fear extends BraitenbergBehaviour {

	@Override
	protected float leftMotorValue() {
		return left.getValue();
	}

	@Override
	protected float rightMotorValue() {
		return right.getValue();
	}

}
