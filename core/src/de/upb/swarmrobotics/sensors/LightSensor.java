package de.upb.swarmrobotics.sensors;

import de.upb.swarmrobotics.core.Environment;
import de.upb.swarmrobotics.core.LightSource;
import de.upb.swarmrobotics.core.SensorPosition;

public class LightSensor extends Sensor<Float> {

	private Environment environment;

	public LightSensor(Environment environment, SensorPosition position) {
		super(position);
		this.environment = environment;
	}

	@Override
	public Float getValue() {
		float minDist = Float.MAX_VALUE;
		for (LightSource ls : environment.getLightSources()) {
			minDist = minDist > ls.getDistance(getPosition()) ? ls.getDistance(getPosition()) : minDist;
		}
		return 1 / minDist;
	}
}
