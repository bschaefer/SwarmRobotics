package de.upb.swarmrobotics.sensors;

import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Vector2;

import de.upb.swarmrobotics.core.Robot;
import de.upb.swarmrobotics.core.SensorPosition;

/**
 * Created on 31.05.2016.
 *
 * @author Henrik
 */
public abstract class Sensor<T> {
	public SensorPosition position = SensorPosition.Center;
	public Robot robot;
	private Matrix3 rotatedToSensor;

	public Sensor() {
	}

	public Sensor(SensorPosition position) {
		this.position = position;
	}

	public void attachTo(Robot robot) {
		this.robot = robot;
		float t_x = (float) (robot.getRadius() * Math.cos(position.angle));
		float t_y = (float) (robot.getRadius() * Math.sin(position.angle));
		rotatedToSensor = new Matrix3(new float[] { 1, 0, 0, 0, 1, 0, t_x, t_y, 1 });
	}

	public abstract T getValue();

	/**
	 * Get the position of the sensor.
	 * 
	 * @return position of the sensor
	 */
	public Vector2 getPosition() {
		float cos_r = (float) Math.cos(robot.getAngle());
		float sin_r = (float) Math.sin(robot.getAngle());
		Vector2 robotPosition = robot.getPosition();
		Matrix3 baseToRobot = new Matrix3(new float[] { 1, 0, 0, 0, 1, 0, robotPosition.x, robotPosition.y, 1 });
		Matrix3 robotRotated = new Matrix3(new float[] { cos_r, sin_r, 0, -sin_r, cos_r, 0, 0, 0, 1 });
		return baseToRobot.mul(robotRotated).mul(rotatedToSensor).getTranslation(new Vector2());
	}
}
