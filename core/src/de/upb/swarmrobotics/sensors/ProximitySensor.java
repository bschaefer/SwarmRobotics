package de.upb.swarmrobotics.sensors;

import com.badlogic.gdx.math.Vector2;

import de.upb.swarmrobotics.core.ProximityBeamPool;
import de.upb.swarmrobotics.core.SensorPosition;
import de.upb.swarmrobotics.utils.Constants;

public class ProximitySensor extends Sensor<Float> {
	public static final float RANGE = Constants.MIN_HEIGHT / Constants.PIXELS_PER_METER;

	private float value = 0;
	private float timeSinceLastBeam = Float.MAX_VALUE;
	private ProximitySensorBeam lastBeam = null;

	public ProximitySensor(SensorPosition position) {
		super(position);
	}

	private void fireBeam() {
		lastBeam = ProximityBeamPool.getInstance().obtain();
		lastBeam.initBeam(getPosition(), getAngle(), this);
	}

	private float getAngle() {
		return (float) ((robot.getAngle() + position.angle) % 2 * Math.PI);
	}

	protected void updateValue(Vector2 pos, ProximitySensorBeam callingBeam) {
		if (callingBeam == lastBeam) {
			value = Math.max(0, 1 - getPosition().dst(pos) / RANGE);
			lastBeam = null;
		}
	}

	@Override
	public Float getValue() {
		return value;
	}

	/**
	 * Fire if over period. If the deltas amount to more than one period an new
	 * beam is fired.
	 * 
	 * @param delta
	 *            seconds since last call
	 * @param period
	 *            period in which beams are fired
	 */
	public void fireIfOverPeriod(float delta, float period) {
		timeSinceLastBeam += delta;
		if (timeSinceLastBeam >= period) {
			fireBeam();
			timeSinceLastBeam = 0;
		}
	}

}
