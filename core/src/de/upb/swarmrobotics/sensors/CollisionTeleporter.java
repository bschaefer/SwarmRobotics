package de.upb.swarmrobotics.sensors;

import com.badlogic.gdx.physics.box2d.Fixture;

import de.upb.swarmrobotics.core.Robot;
import de.upb.swarmrobotics.core.TaskContainer;

/**
 * A pseudo-sensor that can be attached to walls to teleport colliding robots in
 * a specified dimension.
 * 
 * @author ben
 *
 */
public class CollisionTeleporter extends FixtureSensor {

	private TaskContainer tasks = TaskContainer.getInstance();

	private float dimDest;
	private Dimension dim;

	public enum Dimension {
		X, Y
	}

	/**
	 * 
	 * @param dimension
	 *            dimension in which the robot will be teleported
	 * @param destination
	 *            destination in the given dimension (robot radius will be
	 *            considered)
	 */
	public CollisionTeleporter(Dimension dimension, float destination) {
		this.dim = dimension;
		this.dimDest = destination;
	}

	@Override
	public void beginContact(Fixture other) {
		super.beginContact(other);
		Robot robot = null;
		if (other.getUserData() instanceof Robot) {
			robot = (Robot) other.getUserData();
		} else if (other.getUserData() instanceof Sensor) {
			robot = ((Sensor<?>) other.getUserData()).robot;
		}
		if (robot != null) {
			addTeleportationTask(robot);
		}

	}

	/**
	 * Adds a task to the TaskList which is going to be executed out of the
	 * world-step
	 * 
	 * @param robot
	 *            the robot which collided
	 */
	private void addTeleportationTask(final Robot robot) {
		switch (dim) {
		case X:
			tasks.add(() -> robot.setXPosition(getDestinationFromRobotCenter(robot)));
			break;
		case Y:
			tasks.add(() -> robot.setYPosition(getDestinationFromRobotCenter(robot)));
			break;
		}
	}

	/**
	 * Calculates the destination according to the robots radius.
	 * 
	 * @param robot
	 *            the robot which collided
	 * @return the destination
	 */
	private float getDestinationFromRobotCenter(final Robot robot) {
		int sign = (int) Math.signum(dimDest);
		float val = dimDest - sign * robot.getRadius();
		switch (sign) {
		case -1:
			return (float) Math.ceil(val);
		case 1:
			return (float) Math.floor(val);
		default:
			return val;
		}

	}

}
