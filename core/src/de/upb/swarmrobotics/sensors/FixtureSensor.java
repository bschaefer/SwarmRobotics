package de.upb.swarmrobotics.sensors;

import com.badlogic.gdx.physics.box2d.Fixture;

import de.upb.swarmrobotics.core.SensorPosition;

/**
 * Base class physics sensors. It has a counter that is incremented whenever a
 * collision happens and decremented when it dissolves.
 *
 * @author Henrik
 */
public abstract class FixtureSensor extends Sensor<Boolean>{

	public int collisions;

	@Override
	public Boolean getValue() {
		return collisions > 0;
	}

	public FixtureSensor() {
	}

	public FixtureSensor(SensorPosition position) {
		super(position);
	}

	public void beginContact(Fixture other) {
		collisions++;
	}

	public void endContact(Fixture other) {
		collisions--;
	}
}
