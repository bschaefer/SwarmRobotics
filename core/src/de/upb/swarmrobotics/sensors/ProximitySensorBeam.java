package de.upb.swarmrobotics.sensors;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.utils.Pool.Poolable;

import de.upb.swarmrobotics.core.Environment;
import de.upb.swarmrobotics.core.ProximityBeamPool;
import de.upb.swarmrobotics.core.TaskContainer;
import de.upb.swarmrobotics.utils.BodyBuilder;
import de.upb.swarmrobotics.utils.Mask;

/**
 * The beam of a sensor that is moving really to detect obstacles in the line of
 * sight.
 * 
 * @author ben
 *
 */
public class ProximitySensorBeam extends FixtureSensor implements Poolable {
	public static final float SPEED = 100000;

	private ProximitySensor sensor;
	private Body body;
	private boolean active = false;
	private Fixture fixture = null;
	private FixtureDef fixtureDef;

	public ProximitySensorBeam(Environment env) {
		BodyBuilder builder = env.bodyBuilder(true).bullet();
		// TODO different shape or invisible?
		body = builder.build();
		fixtureDef = builder.fixtureDefBuilder().circleShape(.01f).maskBits(Mask.SensorMask).categoryBits(Mask.SENSOR)
				.density(0f).build();
	}

	public void initBeam(Vector2 pos, float angle, ProximitySensor sensor) {
		this.sensor = sensor;
		fixture = body.createFixture(fixtureDef);
		fixture.setUserData(this);
		body.setTransform(pos.add(Math.signum(pos.x) * .1f, Math.signum(pos.y) * .1f), angle);
		body.setLinearVelocity((float) (SPEED * Math.cos(angle)), (float) (SPEED * Math.sin(angle)));
		active = true;
	}

	@Override
	public void beginContact(Fixture other) {
		if (active) {
			super.beginContact(other);
			updateSensorAndDie();

		}
	}

	public void updateSensorAndDie() {
		sensor.updateValue(getPosition(), this);
		ProximityBeamPool.getInstance().free(this);
	}

	@Override
	public Vector2 getPosition() {
		return body.getPosition();
	}

	@Override
	public void reset() {
		this.sensor = null;
		active = false;
		body.setLinearVelocity(0, 0);
		TaskContainer.getInstance().add(() -> body.destroyFixture(fixture));
	}

}
