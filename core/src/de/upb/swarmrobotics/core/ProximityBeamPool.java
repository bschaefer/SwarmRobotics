package de.upb.swarmrobotics.core;

import com.badlogic.gdx.utils.Pool;

import de.upb.swarmrobotics.sensors.ProximitySensorBeam;

public class ProximityBeamPool extends Pool<ProximitySensorBeam> {

	private static ProximityBeamPool instance;

	private Environment environment;

	public static ProximityBeamPool getInstance() {
		if (instance == null) {
			throw new NullPointerException("Instance must be initialized by the environment!");
		}
		return instance;
	}

	protected ProximityBeamPool(Environment env) {
		environment = env;
		instance = this;
	}

	@Override
	protected ProximitySensorBeam newObject() {
		return new ProximitySensorBeam(environment);
	}

}
