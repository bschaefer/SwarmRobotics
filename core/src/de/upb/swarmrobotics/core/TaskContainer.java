package de.upb.swarmrobotics.core;

import java.util.ArrayList;
import java.util.List;

/**
 * A singleton container for tasks that can not be executed during a world-step.
 * 
 * @author ben
 *
 */
public class TaskContainer {

	private static TaskContainer instance;

	private List<TaskOutsideWorldstep> taskList;

	/**
	 * 
	 * @return the singleton instance of the task container.
	 */
	public static TaskContainer getInstance() {
		if (instance == null)
			instance = new TaskContainer();
		return instance;
	}

	private TaskContainer() {
		taskList = new ArrayList<>();
	}

	/**
	 * Adds a task which is going to be executed when the current world-step has
	 * finished.
	 * 
	 * @param task
	 *            the task to be executed
	 */
	public void add(TaskOutsideWorldstep task) {
		taskList.add(task);
	}

	/**
	 * Executes all tasks in the container and deletes them.
	 */
	public void executeAll() {
		for (TaskOutsideWorldstep task : taskList) {
			task.execute();
		}
		taskList.clear();
	}

}
