package de.upb.swarmrobotics.core;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

import de.upb.swarmrobotics.sensors.FixtureSensor;
import de.upb.swarmrobotics.utils.Util;

public class FixtureSensorUpdater implements ContactListener {

	@Override
	public void beginContact(Contact contact) {
		Fixture a = contact.getFixtureA();
		Fixture b = contact.getFixtureB();
		if (Util.isSensor(a))
			((FixtureSensor) a.getUserData()).beginContact(b);
		if (Util.isSensor(b))
			((FixtureSensor) b.getUserData()).beginContact(a);
	}

	@Override
	public void endContact(Contact contact) {
		Fixture a = contact.getFixtureA();
		Fixture b = contact.getFixtureB();
		if (Util.isSensor(a))
			((FixtureSensor) a.getUserData()).endContact(b);
		if (Util.isSensor(b))
			((FixtureSensor) b.getUserData()).endContact(a);
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
	}

}
