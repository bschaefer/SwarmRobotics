package de.upb.swarmrobotics.core;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import de.upb.swarmrobotics.sensors.CollisionTeleporter;
import de.upb.swarmrobotics.sensors.CollisionTeleporter.Dimension;
import de.upb.swarmrobotics.utils.BodyBuilder;
import de.upb.swarmrobotics.utils.Constants;
import de.upb.swarmrobotics.utils.FixtureDefBuilder;

/**
 * Created on 31.05.2016.
 *
 * @author Henrik
 */
public class Environment {
	public final World world;
	public final ExtendViewport viewport;
	public final int width, height;
	private Array<ProactiveWorldObject> proactiveWorldObjects = new Array<>();
	private Array<LightSource> lightSources = new Array<>();
	private final Box2DDebugRenderer debugRenderer;
	private float accumulator = 0;
	private Task task;
	private boolean pause = true;
	private TaskContainer tasksOutsideWorldstep = TaskContainer.getInstance();

	public void pause() {
		pause = true;
	}

	public void unpause() {
		pause = false;
	}

	public void togglePause() {
		pause = !pause;
	}

	/**
	 * @param width
	 *            width of the environment in meters
	 * @param height
	 *            height of the environment in meters
	 * @param task
	 *            the task that will be executed
	 */
	public Environment(int width, int height, Task task) {
		this.width = width;
		this.height = height;
		this.task = task;
		world = new World(new Vector2(), true);
		viewport = new ExtendViewport(width, height);
		world.setContactListener(new FixtureSensorUpdater());
		debugRenderer = new Box2DDebugRenderer();
		new ProximityBeamPool(this);
		// add some walls
		int halfw = width / 2;
		int halfh = height / 2;
		addBoundaries(halfw, halfh);
	}

	/**
	 * Adds boundaries to the world at deltaX and deltaY from the center. The
	 * boundaries implement a torus.
	 * 
	 * @param deltaX
	 *            x-distance from the center
	 * @param deltaY
	 *            y-distance from the center
	 */
	private void addBoundaries(int deltaX, int deltaY) {
		Vector2 bottomLeft = new Vector2(-deltaX, -deltaY);
		Vector2 bottomRight = new Vector2(deltaX, -deltaY);
		Vector2 topLeft = new Vector2(-deltaX, deltaY);
		Vector2 topRight = new Vector2(deltaX, deltaY);

		addWall(bottomLeft, topLeft, Dimension.X, deltaX);
		addWall(bottomRight, topRight, Dimension.X, -deltaX);
		addWall(bottomLeft, bottomRight, Dimension.Y, deltaY);
		addWall(topLeft, topRight, Dimension.Y, -deltaY);
	}

	/**
	 * Add a wall from point a to point b which teleports robots in Dimension d
	 * to to the oppositeWall.
	 * 
	 * @param a
	 *            first point of the wall
	 * @param b
	 *            second point of the wall
	 * @param d
	 *            dimension in which robots are teleported
	 * @param oppositeWall
	 *            the location of the opposite wall in dimension d
	 */
	private void addWall(Vector2 a, Vector2 b, Dimension d, int oppositeWall) {
		BodyBuilder builder = new BodyBuilder(world);
		FixtureDefBuilder fixtureDef = builder.fixtureDefBuilder().edgeShape(a, b);
		if (task.isTorus()) {
			fixtureDef.userData(new CollisionTeleporter(d, oppositeWall));
		}
		builder.type(BodyDef.BodyType.StaticBody).fixture(fixtureDef).build();
	}

	/**
	 * Returns the combined projection and view matrix of the viewport
	 * 
	 * @return
	 */
	public Matrix4 getMatrix() {
		return viewport.getCamera().combined;
	}

	/**
	 * Initializes the task and afterwards all robots
	 * 
	 * @param task
	 */
	public void start() {
		task.onCreate(this);
		for (ProactiveWorldObject proObj : proactiveWorldObjects)
			proObj.build();
	}

	/**
	 * Udates all robots and the physics world. If paused this method will do
	 * nothing
	 * 
	 * @param delta
	 */
	public void update(float delta) {
		if (pause)
			return;
		for (ProactiveWorldObject proObj : proactiveWorldObjects)
			proObj.update(delta);
		doPhysicsStep(delta);
	}

	/**
	 * Renders the world and afterwards it calls task.render
	 */
	public void render() {
		debugRenderer.render(world, getMatrix());
		task.render(getMatrix());
	}

	private void doPhysicsStep(float deltaTime) {
		float frameTime = Math.min(deltaTime, 0.25f);
		accumulator += frameTime;
		while (accumulator >= Constants.TIME_STEP) {
			world.step(Constants.TIME_STEP, Constants.VELOCITY_ITERATIONS, Constants.POSITION_ITERATIONS);
			tasksOutsideWorldstep.executeAll();
			accumulator -= Constants.TIME_STEP;
		}
	}

	/**
	 * Creates a new robot with the given radius and adds it to this environment
	 * 
	 * @param radius
	 * @return
	 */
	public Robot createRobot(float radius) {
		Robot robot = new Robot(this, radius);
		proactiveWorldObjects.add(robot);
		return robot;
	}

	/**
	 * Creates a light source at the given position
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public LightSource createLightSource(float x, float y) {
		LightSource ls = new LightSource(this, x, y);
		lightSources.add(ls);
		return ls;
	}

	public Iterable<LightSource> getLightSources() {
		return lightSources;
	}

	/**
	 * Returns a new body builder.
	 * 
	 * @param dynamic
	 *            true if the body should be a dynamic body otherwise it will be
	 *            a static body
	 * @return
	 */
	public BodyBuilder bodyBuilder(boolean dynamic) {
		return new BodyBuilder(world).type(dynamic ? BodyDef.BodyType.DynamicBody : BodyDef.BodyType.StaticBody);
	}

}
