package de.upb.swarmrobotics.core;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

import de.upb.swarmrobotics.utils.BodyBuilder;
import de.upb.swarmrobotics.utils.Mask;

public class LightSource {

	private Body body;

	public LightSource(Environment environment, float x, float y) {
		BodyBuilder bodyBuilder = environment.bodyBuilder(false).position(x, y).userData(this);
		bodyBuilder.fixture(bodyBuilder.fixtureDefBuilder().circleShape(.1f).categoryBits(Mask.FLYING));
		body = bodyBuilder.build();
	}

	public Vector2 getPosition() {
		return body.getPosition();
	}

	public float getDistance(Vector2 other) {
		return body.getPosition().dst(other);
	}

}
