package de.upb.swarmrobotics.core;

/**
 * A task that can be executed out of a world-step.
 * 
 * @author ben
 *
 */
public interface TaskOutsideWorldstep {

	/**
	 * Execution of the task.
	 */
	public void execute();

}
