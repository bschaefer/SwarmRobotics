package de.upb.swarmrobotics.core;

public interface ProactiveWorldObject {

	/**
	 * Perform interactions that took place since delta seconds.
	 * 
	 * @param delta
	 *            seconds since last call
	 */
	public void update(float delta);
	
	/**
	 * Actually builds the physics body, should only be called once from inside
	 * the environment.
	 */
	public void build();

}
