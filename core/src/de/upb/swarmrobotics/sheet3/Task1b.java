package de.upb.swarmrobotics.sheet3;

import com.badlogic.gdx.math.Matrix4;
import de.upb.swarmrobotics.core.Environment;
import de.upb.swarmrobotics.core.Robot;
import de.upb.swarmrobotics.core.SensorPosition;
import de.upb.swarmrobotics.core.Task;
import de.upb.swarmrobotics.sensors.Bumper;

/**
 * Created on 01.06.2016.
 *
 * @author Henrik
 */
public class Task1b implements Task {
	@Override
	public void onCreate(Environment environment) {
		float r = 0.5f;
		Robot robot = environment.createRobot(r);
		robot.attach(new Bumper(0.25f, -70, r));
		robot.attach(new Bumper(0.25f, r, SensorPosition.FrontLeft));
		robot.attach(new Bumper(0.25f, r, SensorPosition.Front));
		robot.attach(new Bumper(0.25f, r, SensorPosition.FrontRight));
		robot.initialPosition(5, 0);
		robot.attach(new WallFollower());
	}

	@Override
	public void render(Matrix4 mvp) {

	}
}
