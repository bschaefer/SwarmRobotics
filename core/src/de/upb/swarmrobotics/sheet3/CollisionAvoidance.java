package de.upb.swarmrobotics.sheet3;

import de.upb.swarmrobotics.core.Behavior;
import de.upb.swarmrobotics.core.SensorPosition;
import de.upb.swarmrobotics.sensors.Bumper;

/**
 * Created on 31.05.2016.
 *
 * @author Henrik
 */
public class CollisionAvoidance extends Behavior {
    private Bumper left, front, right;

    @Override
    public void onCreate() {
        left = robot.sensor(Bumper.class, SensorPosition.FrontLeft);
        front = robot.sensor(Bumper.class, SensorPosition.Front);
        right = robot.sensor(Bumper.class, SensorPosition.FrontRight);
    }

    @Override
    public void update(float delta) {
        if(left.getValue() || front.getValue() || right.getValue()) {
            robot.setTargetSpeed(0);
            robot.turnToDeg(10);
        } else {
            robot.setTargetSpeed(5);
        }
    }
}
