package de.upb.swarmrobotics.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import de.upb.swarmrobotics.core.Environment;
import de.upb.swarmrobotics.utils.BodyBuilder;
import de.upb.swarmrobotics.utils.Resources;

/**
 * Created on 01.06.2016.
 *
 * @author Henrik
 */
public class ToolWindow extends Window {
    private ShapeRenderer renderer;
    private Environment environment;
    private TextField widthField;
    private TextField heightField;
    private TextField radiusField;
    private TextField angleField;
    private boolean placeRect = true;

    public ToolWindow(final Environment environment) {
        super("Tools", Resources.skin);
        this.environment = environment;
        renderer = new ShapeRenderer();

        TextButton button = new TextButton("Pause", Resources.skin);
        button.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) { environment.pause(); }
        });
        add(button);
        button = new TextButton("Play", Resources.skin);
        button.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) { environment.unpause(); }
        });
        add(button).row();

        button = new TextButton("Rect", Resources.skin);
        button.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) { placeRect = true; }
        });
        add(button);
        button = new TextButton("Turn 45°", Resources.skin);
        button.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) { turn(); }
        });
        add(button).row();
        add(new Label("Width:", Resources.skin));
        add(widthField = createField(1)).row();
        add(new Label("Height:", Resources.skin));
        add(heightField = createField(1)).row();
        add(new Label("Angle:", Resources.skin));
        add(angleField = createField(0)).row();
        row();

        button = new TextButton("Circle", Resources.skin);
        button.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) { placeRect = false; }
        });
        add(button).row();
        add(new Label("Radius:", Resources.skin));
        add(radiusField = createField(0.5f)).row();

        pack();
    }

    private TextField createField(float value) {
        TextField field = new TextField(Float.toString(value), Resources.skin);
        field.setTextFieldFilter(FloatInputFilter.instance);
        return field;
    }

    private void turn() {
        angleField.setText(Float.toString((parseField(angleField, 0) + 45) % 360));
    }

    private float parseField(TextField field, float defaultValue) {
        try {
            return Float.parseFloat(field.getText());
        } catch(NumberFormatException ex) {}
        return defaultValue;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        //batchrendering is active, so we have to temporary disable it
        batch.end();
        renderPreview();
        batch.begin();
    }

    private void renderPreview() {
        Vector2 pos = screenToWorld(Gdx.input.getX(), Gdx.input.getY());
        renderer.setProjectionMatrix(environment.getMatrix());
        renderer.begin(ShapeRenderer.ShapeType.Line);
        if(placeRect) {
            float w = parseField(widthField, 1);
            float h = parseField(heightField, 1);
            float angle = parseField(angleField, 0);
            renderer.rect(pos.x - w/2, pos.y - h/2, w/2, h/2, w, h, 1, 1, angle);
        } else {
            float r = parseField(radiusField, 0.5f);
            renderer.circle(pos.x, pos.y, r, 16);
        }
        renderer.end();
    }

    public void handleTouch(int screenX, int screenY) {
        Vector2 pos = screenToWorld(screenX, screenY);
        BodyBuilder builder = environment.bodyBuilder(false).position(pos.x, pos.y);
        if(placeRect) {
            float w = parseField(widthField, 1);
            float h = parseField(heightField, 1);
            float angle = MathUtils.degRad * parseField(angleField, 0);
            builder.fixture(builder.fixtureDefBuilder().boxShape(w/2, h/2)).angle(angle).build();
        } else {
            float r = parseField(radiusField, 0.5f);
            builder.fixture(builder.fixtureDefBuilder().circleShape(r)).build();
        }
    }

    private Vector2 screenToWorld(int x, int y) {
        Vector3 pos = new Vector3(x, y, 0);
        environment.viewport.getCamera().unproject(pos);
        return new Vector2(pos.x, pos.y);
    }
}
