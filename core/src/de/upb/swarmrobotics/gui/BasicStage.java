package de.upb.swarmrobotics.gui;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import de.upb.swarmrobotics.core.Environment;

/**
 * UI base class. Escape key will toggle its visibility and Enter key will (un)pause the environment simulation.
 * Any touch input that doesn't have a target, will be redirected to ToolWindow.handleTouch
 *
 * @author Henrik
 */
public class BasicStage extends Stage {
    private Environment environment;
    private ToolWindow tools;
    private boolean visible = true;
    public void toggleVisibility() { visible = !visible; }

    public BasicStage(Environment environment) {
        this.environment = environment;
        addActor(tools = new ToolWindow(environment));
    }

    @Override
    public void draw() {
        if(visible) super.draw();
    }

    @Override
    public boolean keyDown(int keyCode) {
        return visible && super.keyDown(keyCode);
    }

    @Override
    public boolean keyTyped(char character) {
        return visible && super.keyTyped(character);
    }

    @Override
    public boolean keyUp(int keyCode) {
        if(keyCode == Input.Keys.ESCAPE) toggleVisibility();
        if(keyCode == Input.Keys.ENTER) environment.togglePause();
        return visible && super.keyUp(keyCode);
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return visible && super.mouseMoved(screenX, screenY);
    }

    @Override
    public boolean scrolled(int amount) {
        return visible && super.scrolled(amount);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return visible && super.touchDown(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return visible && super.touchDragged(screenX, screenY, pointer);
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if(!visible) return false;
        Vector2 screen = new Vector2(screenX, screenY);
        screenToStageCoordinates(screen);
        if(hit(screen.x, screen.y, true) == null) tools.handleTouch(screenX, screenY);
        return super.touchUp(screenX, screenY, pointer, button);
    }
}
