package de.upb.swarmrobotics.utils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

import java.util.ArrayList;

/**
 * Utility class used to construct a Box2D physics body.
 * Most methods will return the same BodyBuilder instance.
 * Therefore you can chain method calls like this:
 * <pre>builder.position(10, 10).angle(3.141f).mass(5)</pre>
 * @author Henrik
 */
public class BodyBuilder {
    private BodyDef bodyDef;
    private final ArrayList<Tuple> tuples = new ArrayList<Tuple>();
    private float mass = 1f;
    private Object userData = null;
    private final Vector2 position = new Vector2();
    private float angle;
    private World world;

    private final FixtureDefBuilder fixtureDefBuilder = new FixtureDefBuilder();

    public BodyBuilder(World world) {
        this.world = world;
        reset();
    }

    public FixtureDefBuilder fixtureDefBuilder() { return fixtureDefBuilder; }

    private void reset() {
        for(Tuple tuple : tuples)
            tuple.fixtureDef.shape.dispose();

        bodyDef = new BodyDef();
        tuples.clear();
        mass = 1f;
        angle = 0f;
        userData = null;
        position.set(0f, 0f);
    }

    public BodyBuilder linearDamping(float value) {
        bodyDef.linearDamping = value;
        return this;
    }

    public BodyBuilder angularDamping(float value) {
        bodyDef.angularDamping = value;
        return this;
    }

    public BodyBuilder type(BodyDef.BodyType type) {
        bodyDef.type = type;
        return this;
    }

    public BodyBuilder bullet() {
        bodyDef.bullet = true;
        return this;
    }

    public BodyBuilder fixedRotation() {
        bodyDef.fixedRotation = true;
        return this;
    }

    public BodyBuilder fixture(FixtureDefBuilder fixtureDef) {
        tuples.add(new Tuple(fixtureDef));
        return this;
    }

    public BodyBuilder mass(float mass) {
        this.mass = mass;
        return this;
    }

    public BodyBuilder userData(Object userData) {
        this.userData = userData;
        return this;
    }

    public BodyBuilder position(float x, float y) {
        this.position.set(x, y);
        return this;
    }

    public BodyBuilder angle(float angle) {
        this.angle = angle;
        return this;
    }

    /**
     * This will actually build a concrete physics body instance.
     * Afterwards this builder will automatically be reset and can be used for the next object.
     * @return
     */
    public Body build() {
        Body body = world.createBody(bodyDef);

        for(Tuple tuple : tuples)
            body.createFixture(tuple.fixtureDef).setUserData(tuple.data);

        MassData massData = body.getMassData();
        massData.mass = mass;
        body.setMassData(massData);
        body.setUserData(userData);
        body.setTransform(position, angle);
        reset();
        return body;
    }

    /**
     * Utility class used to map userdata to fixtures
     */
    private static class Tuple {
        public FixtureDef fixtureDef;
        public Object data;

        public Tuple(FixtureDefBuilder builder) {
            data = builder.userData;
            fixtureDef = builder.build();
        }
    }
}
