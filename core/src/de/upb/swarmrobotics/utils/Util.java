package de.upb.swarmrobotics.utils;

import com.badlogic.gdx.physics.box2d.Fixture;

import de.upb.swarmrobotics.core.Robot;
import de.upb.swarmrobotics.sensors.FixtureSensor;

/**
 * Created on 31.05.2016.
 *
 * @author Henrik
 */
public class Util {
    /**
     * Returns true if the given fixture has userdata and this data is a FixtureSensor
     * @param fixture
     * @return
     */
    public static boolean isSensor(Fixture fixture) {
        Object data = fixture.getUserData();
        return data != null && data instanceof FixtureSensor;
    }
    /**
     * Returns true if the given fixture has userdata and this data is a Robot
     * @param fixture
     * @return
     */
    public static boolean isRobot(Fixture fixture) {
        Object data = fixture.getUserData();
        return data != null && data instanceof Robot;
    }
}
